<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
/* В задании не было указано как именно будут добавляться товары и предложения
* поэтому написала импорт из файла(пример файла прикладываю)
*/
$row = 1;
$catalog_id = 39;
$catalog_sku_id = 40;

if (($handle = fopen("upload/import_files/test.csv", "r")) !== FALSE) {

	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

        if($row == 1) {
        	$row++;
        	continue;
        }

        $fields = Array("NAME" => $data[0], "IBLOCK_ID" => $catalog_id);
        $check_res = CIBlockElement::GetList(
						Array("SORT"=>"ASC"),
						$fields,
						false,
						false,
						Array()
					);
        $check = $check_res->GetNextElement();
        if(empty($check)) { //если товара нет

        	$detail_text = $data[2];

        	$arLoadProductArray = Array(
			  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
			  "IBLOCK_ID"      => $catalog_id,
			  "NAME"           => $data[0],
			  "PREVIEW_TEXT"   => "",
			  "DETAIL_TEXT_TYPE" => "text",
			  "DETAIL_TEXT"    => $detail_text,
			);
			$arParams = array(
		      "max_len" => "100", 
		      "change_case" => "L", 
		      "replace_space" => "-", 
		      "replace_other" => "-", 
		      "delete_repeat_replace" => "true", 
		      "use_google" => "false",
		    );
		    $arLoadProductArray["CODE"] = Cutil::translit($arLoadProductArray["NAME"], "ru", $arParams);
			$arLoadProductArray["ACTIVE"] = "Y";
			if(!empty($data[3])){
				$photo = CFile::MakeFileArray($data[3]);
				$arLoadProductArray["DETAIL_PICTURE"] = $photo;
				$arLoadProductArray["PREVIEW_PICTURE"] = $photo;
			}

			$el = new CIBlockElement;
			if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
					
				if (CModule::IncludeModule('sale')) {

					$arFields = Array(
					    "PRODUCT_ID" => $PRODUCT_ID,
					    "CATALOG_GROUP_ID" => 1,
					    "PRICE" => $data[1],
					    "CURRENCY" => "RUB",
					);
					CPrice::Add($arFields, true);
				}
				
				if($data[4] == 'Y') {
			  	
					Cmodule::IncludeModule('catalog');
					CCatalogStoreProduct::Add(array('PRODUCT_ID' => $PRODUCT_ID, 'STORE_ID' => '1', 'AMOUNT' => '10'));
					$count = CCatalogProduct::Add(array('ID' => $PRODUCT_ID, 'AVAILABLE' => 'Y', 'QUANTITY' => 11));

				}
			} else
				echo "Error: ".$el->LAST_ERROR;

        } else {
        	$PRODUCT_ID = $check->fields['ID'];
        }

        //предложения
        if(!empty($PRODUCT_ID) && !empty($data[1]) && !empty($data[4]) && !empty($data[5]) && !empty($data[5])) {
        	
        	$obElement = new CIBlockElement();
        	$arProp['CML2_LINK'] = $PRODUCT_ID;

		   	$arParams = array(
		      "max_len" => "100", 
		      "change_case" => "L", 
		      "replace_space" => "-", 
		      "replace_other" => "-", 
		      "delete_repeat_replace" => "true", 
		      "use_google" => "false",
		    );
		   	$arFields = array(
		      'NAME' => $data[0],
		      'CODE' => Cutil::translit($data[0], "ru", $arParams),
		      'IBLOCK_ID' => $catalog_sku_id,
		      'ACTIVE' => 'Y',
		      'PROPERTY_VALUES' => $arProp
		   	);
		   	$intOfferID = $obElement->Add($arFields);

		   	
		   	if(!empty($data[5])) {
				
			    CIBlockElement::SetPropertyValueCode(
					$intOfferID,
				 	'ARTICLE',
				 	trim($data[5])
				);

		   	}

		   	if(!empty($data[6])) {
		   		$sizes = CIBlockPropertyEnum::GetList(
					Array("SORT"=>"ASC", "VALUE"=>"ASC"),
					Array("IBLOCK_ID"=>$IBLOCK_ID, "CODE"=>"SIZE", "VALUE" => $data[6])
				);
				$sizes = $sizes->GetNext();
				if(!$sizes) {//надо задать ID свойства торгового предложения
					CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>1030, 'VALUE'=>$data[6]));
					
					$sizes = CIBlockPropertyEnum::GetList(
						Array("SORT"=>"ASC", "VALUE"=>"ASC"),
						Array("IBLOCK_ID"=>$IBLOCK_ID, "CODE"=>"SIZE", "VALUE" => $data[6])
					);
					$sizes = $sizes->GetNext();
				}

				CIBlockElement::SetPropertyValueCode(
					$intOfferID,
				 	'SIZE',
				 	$sizes['ID']
				);
		   	}

		   	if(Cmodule::IncludeModule('catalog')) {

			   	if(!empty(trim($data[1]))) {

					$price = $data[1];
					
					
			   		if (CModule::IncludeModule('sale')) {

						
						$arFields = Array(
						    "PRODUCT_ID" => $intOfferID,
						    "CATALOG_GROUP_ID" => 1,
						    "PRICE" => $price,
						    "CURRENCY" => "RUB",
						);
						$price_test = CPrice::Add($arFields, true);
						if($data[4] == 'Y') {
		   					$test = CCatalogProduct::Add(array('ID' => $intOfferID, 'AVAILABLE' => 'Y', 'QUANTITY' => 10));
						}
					}
				}
		   	}
        }

		$row++;
	}
}