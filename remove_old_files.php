<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $DB;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

//Целевая папка для поиска файлов
$rootDirPath = $_SERVER['DOCUMENT_ROOT'] . "/upload/iblock";

$time_start = microtime(true);

// Получаем записи из таблицы b_file
$arFilesCache = array();
$result = $DB->Query('SELECT FILE_NAME, SUBDIR FROM b_file WHERE MODULE_ID = "iblock"');
while ($row = $result->Fetch()) {
    $arFilesCache[$row['FILE_NAME']] = $row['SUBDIR'];
}
$hRootDir = opendir($rootDirPath);
$count = 0;
$contDir = 0;
$countFile = 0;
$i = 1;
$removeFile=0;
while (false !== ($subDirName = readdir($hRootDir))) {
    if ($subDirName == '.' || $subDirName == '..') {
        continue;
    }
   
    $filesCount = 0;
    $subDirPath = "$rootDirPath/$subDirName"; 
    $hSubDir = opendir($subDirPath);
    while (false !== ($fileName = readdir($hSubDir))) {
        if ($fileName == '.' || $fileName == '..') {
            continue;
        }
        $countFile++;
        if (array_key_exists($fileName, $arFilesCache)) { //если файл есть и в папке и в базе пропускаем
            $filesCount++;
            continue;
        }
        $fullPath = "$subDirPath/$fileName"; 
           
        //Удаление файла
        if (unlink($fullPath)) {
            $removeFile++;
        }
        $i++;
        $count++;
        unset($fileName);
    }
    closedir($hSubDir);
    //Удалить поддиректорию, если удаление активно и счётчик файлов пустой - т.е каталог пуст
    if ($deleteFiles && !$filesCount) {
        rmdir($subDirPath);
    }
    $contDir++;
}
echo 'Всего файлов удалил: <strong>' . $removeFile . '</strong><br>';
echo 'Всего файлов в ' . $rootDirPath . ': <strong>' . $count . '</strong><br>';
echo 'Всего подкаталогов в ' . $rootDirPath . ': <strong>' . $contDir . '</strong><br>';
echo 'Всего записей в b_file: <strong>' . count($arFilesCache) . '</strong><br>';
closedir($hRootDir);

echo '<br>';
$time_end = microtime(true);
$time = $time_end - $time_start;
 
echo "Время выполнения $time секунд\n";