<?
if (($handle = file_get_contents("chart2.json")) !== FALSE) {
	$json_decode = json_decode($handle);

	$i = 1;
	while (!empty($json_decode[0][$i])) {
		$json_decode = setNull($json_decode, $i);
		$i++;
	}

	$json_encode = json_encode($json_decode);
	$fp = fopen("json_result.json", 'w');
	
	fwrite($fp, $json_encode);
	fclose($fp);
	
}

function setNull($json_decode, $i) {
	$count = 0; //счетчик 100 идущих подряд
	$keysToNullInARow = array();
	$keysToNull = array();
	foreach($json_decode as $key => $item ) {
		if($item[$i] == 100) {
			$count++;
			$keysToNullInARow[] = $key;
		} else {
			if($count>3) 
				$keysToNull = array_merge($keysToNull, $keysToNullInARow);
			$keysToNullInARow = array();
			$count = 0;
		}
	}

	if(!empty($keysToNull)) {
		foreach($keysToNull as $key) {
			$json_decode[$key][$i] = null;
		}
	}

	return $json_decode;
}

